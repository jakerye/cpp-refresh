#ifndef _CALCULATOR_H_
#define _CALCULATOR_H_

class Calculator
{
    public:
        Calculator();
        ~Calculator();

        int add(int num1, int num2);
        float divide(float numerator, float denominator);

        bool getAllowNegatives();
        void setAllowNegatives(bool inValue);

    protected:
        bool fAllowNegatives;
};

#endif // _CALCULATOR_H_