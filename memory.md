# Memory
- https://www.geeksforgeeks.org/memory-layout-of-c-program/
- https://www.geeksforgeeks.org/stack-vs-heap-memory-allocation/

## Text Segment (e.g. text)
- Contains source code (e.g. .cpp, .h files)
- Also known as code segment
- Contains executable instructions
- Usually read-only to prevent program from accidentally modifying instructions

## Initialized Data Segment (e.g. data)
- Contains global and static variables initialized to non-zero values
- NOT read-only since values can be altered at run time

## Uninitialized Data Segment (e.g. bss)
- Contains global and static variables initialized to zero or do not have explicit initializtion in the source code.
- Initialize by kernal to arithmetic zero before program starts execution

## Stack Segment
- Contains the program stack, a LIFO structure
- Top of the stack is tracked by a "stack pointer" register
- Values get pushed to and popped from the stack
- The set of values pushed to the stack for one function call is termed a "stack frame"
- Stores automatic and temporary variables
- Automatic variables include function return addresses and info about the callers environment (e.g. machine registers)
- Allocation and deallocation is automatically handled
- Susceptible to memory shortage
- Alloted memory cannot be altered
- Access time is faster
- Standard arrays live on the stack and their size is determined at compile time (e.g `int* myArray[30]`)

## Heap Segment
- Segment where dynamic memory allocation usually takes place
- Managed by malloc, realloc, and free
- Shared by all shared libraries and dynamically loaded modules
- Allocation and deallocation is handled by the programmer
- Susceptible to memory fragmentation
- Allotted memory can be altered
- Access time is slower
- An array whose size is determined at runtime gets declared on the heap by allocating new memory (e.g. `int* myArray = new int[arraySize]`) which must then be deleted (e.g. `delete[] myArray`)
