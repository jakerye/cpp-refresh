#include <iostream>

namespace mycode {
    
    void foo() {
        std::cout << "foo() is in the mycode namespace" << std::endl;
    }
}
