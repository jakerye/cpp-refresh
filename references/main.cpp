#include <iostream>

void doesntAddOne(int i)
{
    i++;
}

void addOne(int &i)
{ // Note the reference
    i++;
}

int main(int argv, char **argc)
{
    int i = 1;
    addOne(i);
    std::cout << i << std::endl;
    return 0;
}
