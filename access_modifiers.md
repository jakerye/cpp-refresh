# Access Modifiers
- https://www.geeksforgeeks.org/access-modifiers-in-c/

## Public
- Available to everyone

## Private
- Can only be accessed by functions inside the class

## Protected
- Inaccesible outside the class but can be accessed by by any subclass
